from tempfile import mkstemp
from shutil import move
from os import remove, close
import os
import re
import shutil
from copy import *


def copy_and_overwrite(from_path, to_path):
    if os.path.exists(to_path):
        shutil.rmtree(to_path)
    shutil.copytree(from_path, to_path)


def replace(file_path, pattern, subst):
    if not os.path.exists(file_path):
        return
    
    print("replace " + pattern + " " + os.path.abspath(file_path))
    # Create temp file
    fh, abs_path = mkstemp()
    with open(abs_path,'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                new_file.write(line.replace(pattern, subst))
    close(fh)

    # Remove original file
    remove(file_path)

    # Move new file
    move(abs_path, file_path)


def replace_by_regex(file_path, search_pattern, replacement_string):
    replace_regex_by_format(file_path, search_pattern, replacement_string, [])


def replace_regex_by_format(file_path, pattern, template, group_indices):
    print("replaceRegexByFormat " + pattern + " " + os.path.abspath(file_path))
    fh, abs_path = mkstemp()
    with open(abs_path, 'w') as new_file:
        with open(file_path, 'r') as old_file:
            file_string = old_file.read()
            output_string = deepcopy(file_string)
            for m_match in re.finditer(pattern, file_string):
                string_list = []
                for index in group_indices:
                    string_list.append(m_match.group(index).strip())
                replacement_string = template.format(*string_list)
                output_string = output_string.replace(m_match.group(0), replacement_string)
            new_file.write(output_string)
    close(fh)
    remove(file_path)
    move(abs_path, file_path)


#  replace('./com/fasterxml/jackson/core/ObjectCodec.cs', 'public abstract override T readTree<T>(com.fasterxml.jackson.core.JsonParser jp)', 'public abstract override T readTree<T>(com.fasterxml.jackson.core.JsonParser jp);')
def insert_line(file_path, line_number, insertion_string):
    print('insertLine ' + os.path.abspath(file_path))
    # Create temp file
    fh, abs_path = mkstemp()
    counter = 1
    with open(abs_path,'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                if counter >= line_number:
                    new_file.write(insertion_string)
                new_file.write(line)
                counter += 1
    close(fh)
    # Remove original file
    remove(file_path)
    # Move new file
    move(abs_path, file_path)


def comment_out_lines(file_path, line_start, line_end_inclusive, comment_char):
    print('commentOutLines ' + os.path.abspath(file_path))
    # Create temp file
    fh, abs_path = mkstemp()
    counter = 1
    with open(abs_path, 'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                if line_start <= counter <= line_end_inclusive:
                    new_file.write(comment_char + line)
                else:
                    new_file.write(line)
                counter += 1
    close(fh)
    # Remove original file
    remove(file_path)
    # Move new file
    move(abs_path, file_path)
