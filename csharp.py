from tempfile import mkstemp
from shutil import move
from os import remove, close
import os
import re
from copy import *
from generic import replace_regex_by_format
from generic import replace
import generic


def insert_seek(file_path):
    print("insertSeek " + os.path.abspath(file_path))
    # Create temp file
    pattern = r'((int |)offset[ \t]*([\+]*)\=[ \t]*([A-Za-z0-9_ *+]+);)'
    template_current = 'shapeBuffer.BaseStream.Seek({0}, System.IO.SeekOrigin.Current);'
    template_begin = 'shapeBuffer.BaseStream.Seek({0}, System.IO.SeekOrigin.Begin);'
    fh, abs_path = mkstemp()
    previous_line = None

    with open(abs_path, 'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                m = re.search(pattern, line)
                if m is not None and previous_line is not None and ".Read" not in previous_line:
                    replacement_string = ''
                    if len(m.group(3)) == 0:
                        replacement_string = template_begin.format(m.group(4))
                    else:
                        replacement_string = template_current.format(m.group(4))
                    new_file.write(line.replace(m.group(0), replacement_string))
                new_file.write(line)
                previous_line = line

    close(fh)
    # Remove original file
    remove(file_path)
    # Move new file
    move(abs_path, file_path)


def replace_substring(file_path):
    pattern = r'(Sharpen\.Runtime\.Substring\(([A-Za-z_0-9 ]+),([A-Za-z_0-9 ]+),([A-Za-z_0-9\- ]+)\))'
    template = '{0}.Substring({1}, {2} - {1})'
    group_indices = [2, 3, 4]
    replace_regex_by_format(file_path, pattern, template, group_indices)


def replace_import_from_esri(file_path):
    pattern = r'((ImportFromESRIShape[A-Za-z]*)\(([a-zA-Z0-9_, ]*)(System\.IO\.MemoryStream)[\s]+([a-zA-Z0-9_]+)\))'
    template = '{0}({1} System.IO.BinaryReader {2})'
    group_indices = [2, 3, 5]
    replace_regex_by_format(file_path, pattern, template, group_indices)


def replace_export_to_esri(file_path):
    pattern = r'((Export[A-Za-z0-9_]*ToESRIShape)\(([a-zA-Z0-9_,\. ]*)(System\.IO\.MemoryStream)[\s]+([a-zA-Z0-9_]+)\))'
    template = '{0}({1} System.IO.BinaryWriter {2})'
    group_indices = [2, 3, 5]
    replace_regex_by_format(file_path, pattern, template, group_indices)

# System.IO.MemoryStream.Wrap(esriShapeBuffer).Order(java.nio.ByteOrder.LITTLE_ENDIAN)
# System.IO.MemoryStream.Allocate(size).Order(java.nio.ByteOrder.LITTLE_ENDIAN);


def replace_memory_init(file_path):
    pattern = r'((System\.IO\.MemoryStream\.Wrap|System\.IO\.MemoryStream\.Allocate)\(([A-Za-z0-9_]+)\)\.Order\(java\.nio\.ByteOrder\.LITTLE_ENDIAN\))'
    template = 'new System.IO.MemoryStream({0})'
    group_indices = [3]
    replace_regex_by_format(file_path, pattern, template, group_indices)


def replace_new_string(file_path):
    pattern = r'(new[\s]+string\(\"([A-Za-z0-9_\s\(\)\,]+)\"\);[\s]*\n)'
    template = '"{0}";\n'
    group_indices = [2]
    replace_regex_by_format(file_path, pattern, template, group_indices)


def replace_binary_put(file_path):
    pattern = r'(([A-Za-z_][A-Za-z0-9_]*)\.(Put[A-Za-z0-9_]*)\(([A-Za-z0-9_\.\(\)\s:?\'\"]+),[\s]*([A-Za-z0-9_\.\(\)\s:?\'\"]+)\);)'
    template = '{0}.Write({1});'
    group_indices = [2, 5]
    replace_regex_by_format(file_path, pattern, template, group_indices)


def replace_sublist(file_path):
    pattern = r'(([A-Za-z_][A-Za-z0-9_]*)\.SubList\(([A-Za-z0-9_]+)[\s]*,[\s]*([A-Za-z0-9_]+)\))(?=[;.])'
    template = '{0}.GetRange({1}, {2} - {1})'
    group_indices = [2, 3, 4]
    replace_regex_by_format(file_path, pattern, template, group_indices)


# def replace_sublist(file_path):
#     pattern = r'(([A-Za-z_][A-Za-z0-9_]*)\.SubList\(([A-Za-z0-9_]+)[\s]*,[\s]*([A-Za-z0-9_]+)\))(?=[;.])'
#     template = '{0}.GetRange({1}, {2} - {1})'
#     group_indices = [2, 3, 4]
#     replace_regex_by_format(file_path, pattern, template, group_indices)


def replace_as_list(file_path):
    # this could be improved to accept any input type
    pattern = r'(java\.util\.Arrays\.AsList\(([A-Za-z_][A-Za-z_0-9]+)\))(?=[\s]*;)'
    template = '{0}.ToList()'
    # the input type converted to the method executing the ToList method
    group_indices = [2]
    replace_regex_by_format(file_path, pattern, template, group_indices)
    # insert using Linq;
    replace(file_path, 'using Sharpen;', 'using System.Linq;')


def replace_is_empty(file_path):
    pattern = r'([A-Za-z_][A-Za-z0-9_]+\.)(IsEmpty\(\))'
    template = '({0}Count == 0)'
    group_indices = [1]
    replace_regex_by_format(file_path, pattern, template, group_indices)


def replace_byte_buffer(file_path):
    pattern = r'(byteBuffer\.([\w]+)\(([\S]+))(?=\)[\s]*;)'
    template = 'byteBuffer.Write({0}'
    group_indices = [3]
    replace_regex_by_format(file_path, pattern, template, group_indices)


def replace_region_matches(file_path):
    print('replaceRegionMatches ' + os.path.abspath(file_path))
    # TODO warning, this does not handle the case where there are method calls and property access within the RegionMatches method call
    m_search = r'([A-Za-z_][A-Za-z0-9_]*)\.RegionMatches\([\s]*([^,]*)[\s]*,[\s]*([^,]*)[\s]*,[\s]*([^,]*),[\s]*[A-Za-z0-9_]+[\s]*,[\s]*[A-Za-z0-9_]+[\s]*\)'
    fh, abs_path = mkstemp()
    with open(abs_path, 'w') as new_file:
        with open(file_path, 'r') as old_file:
            file_string = old_file.read()
            output_string = deepcopy(file_string)
            for m_match in re.finditer(m_search, file_string):
                string_object = m_match.group(1)
                # TODO warning, this does not catch the case where m_match.group(2) is a variable defined elsewhere
                string_comparison_type = 'System.StringComparison.OrdinalIgnoreCase' if m_match.group(2) == 'true' else 'System.StringComparison.Ordinal'
                start_token = m_match.group(3)
                comparison_string = m_match.group(4)
                # extra bracket in case RegionMatches method call was being negated with '!'
                replacement_string = "(" + string_object + ".IndexOf(" + comparison_string + ", " + start_token + ", " + comparison_string+".Length, " + string_comparison_type + ") > -1)"
                output_string = output_string.replace(m_match.group(0), replacement_string)
            new_file.write(output_string)
    close(fh)
    remove(file_path)
    move(abs_path, file_path)


def comment_out_lines(file_path, line_start, line_end_inclusive):
    generic.comment_out_lines(file_path, line_start, line_end_inclusive, '//')

# def replaceSet(file_path):
#     pattern = r'(([A-Za-z_][A-Za-z0-9_]*)\.(InsertReplace)\(([A-Za-z0-9_\-\+\*\(\)\s\.\[\]]+),([A-Za-z0-9_\-\+\(\)\s\.\[\]\,]+)[\s]*[\)]{1}[\s]*)(?=[;]{1})'
#     template = '{0}[{1}] = {2}'
#     group_indices = [2, 4, 5]
#     replaceRegexByFormat(file_path, pattern, template, group_indices)

# def replaceStaticSubstring(file_path):
#     print('replaceStaticSubstring ' + os.path.abspath(file_path))
#     #method search
#     m_search = r'([a-zA-Z_][a-zA-Z0-9_]*[.])*(Substring[\s]*(?=\())(\()([\sA-Za-z0-9_\-+*\/\.,\(\)]*)(\);)'
#     #variable search
#     # omg it's a monster!!!
#     v_search = r'([\s]*([a-zA-Z_0-9][a-zA-Z0-9_\s\-\+\\\*]*([\.]*[a-zA-Z_][a-zA-Z0-9_\s\-\+\\\*]*(\(([A-Za-z0-9_]*)\)|[A-Za-z0-9_]+))*)[\s]*(?=[,\)]))'
#     fh, abs_path = mkstemp()
#     with open(abs_path, 'w') as new_file:
#         with open(file_path, 'r') as old_file:
#             file_string = old_file.read()
#             outputString = deepcopy(file_string)
#             for m_match in reversed(list(re.finditer(m_search, file_string))):
#                 variableCount = 0
#                 replacement_string = ""
#                 object_name = ""
#                 substring_start = ""
#                 substring_length = ""
#                 final_pos = 0
#                 for v_match in re.finditer(v_search, m_match.group(0)):
#                     if variableCount == 0:
#                         object_name = v_match.group(0).strip()
#                     elif variableCount == 1:
#                         substring_start = v_match.group(0).strip()
#                     elif variableCount == 2:
#                         substring_length = v_match.group(0).strip() + " - " + substring_start.replace("+", "-")
#                         final_pos = v_match.end() + m_match.start()
#                     variableCount = variableCount + 1

#                 if (len(m_match.group(4)) < 3):
#                     continue

#                 replacement_string = object_name + "." + m_match.group(2) + "(" + substring_start +", " + substring_length
#                 outputString = outputString.replace(file_string[m_match.start():final_pos], replacement_string)

#             new_file.write(outputString)
#     close(fh)
#     remove(file_path)
#     move(abs_path, file_path)
